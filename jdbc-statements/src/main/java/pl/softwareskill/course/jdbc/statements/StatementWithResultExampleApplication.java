package pl.softwareskill.course.jdbc.statements;

/**
 * Kod demonstracyjny
 *
 * Aplikacja prezentuje tryb wywołania z wynikiem/rezultatem excuteQuery
 *
 * Uruchomienie nie wymaga podawania parametrów
 */
public class StatementWithResultExampleApplication {

    public static void main(String[] args) throws Exception {
        //Nawiązanie połączenia (PostgreSQL + Hikari, parametry w pliku datasource.properties)
        var connection = HikariDatasourceConnection.getConnection();

        try (var statement = connection.createStatement()) {
            //Zapytanie SQL
            String sqlQuery = "SELECT AMOUNT_TO_PAY FROM ORDERS WHERE ORDER_NUMBER='1'";

            //Wykonanie zapytania z rezultatem
            try (var resulSet = statement.executeQuery(sqlQuery)) {
                if (resulSet.next()) {
                    System.out.println("Znaleziono kwotę do zapłaty i jest równa " + resulSet.getBigDecimal(1));
                } else {
                    System.err.println("Nie znaleziono kwoty do zapłaty");
                }
            }
        }

        //Zwolnienie połączenia
        connection.close();

        //Zwolnienie puli
        HikariDatasourceConnection.close();
    }
}
