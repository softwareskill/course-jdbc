package pl.softwareskill.course.jdbc.statements;

/**
 * Kod demonstracyjny
 *
 * Aplikacja prezentuje tryb wywołania wyrażenia z aktualizacją - executeUpdate oraz liczbę zmodyfikowanych
 * (zaktualizowanych) wierszy.
 *
 * Uruchomienie nie wymaga podawania parametrów
 */
public class StatementUpdatingExampleApplication {

    public static void main(String[] args) throws Exception {
        //Nawiązanie połączenia (PostgreSQL + Hikari, parametry w pliku datasource.properties)
        var connection = HikariDatasourceConnection.getConnection();

        try (var statement = connection.createStatement()) {
            //Zapytanie SQL
            String sqlQuery = "UPDATE ORDERS SET AMOUNT_TO_PAY=12.67";

            //Wykonanie zapytania z pobraniem liczby zmodyfikowanych wierszy
            var count = statement.executeUpdate(sqlQuery);

            //Wiemy że instrukcja to UPDATE true oznacza że zmodyfikowano wiersze, false brak modyfikacji
            if (count > 0) {
                System.out.println("Zaktualizowano kwotę do zapłaty dla " + count + " wierszy");
            } else {
                System.err.println("Nie znaleziono rekordu do aktualizacji kwoty do zapłaty ");
            }
        }

        //Zwolnienie połączenia
        connection.close();

        //Zwolnienie puli
        HikariDatasourceConnection.close();
    }
}
