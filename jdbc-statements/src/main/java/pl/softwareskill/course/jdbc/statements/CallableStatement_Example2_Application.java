package pl.softwareskill.course.jdbc.statements;

import java.sql.Types;

/**
 * Kod demonstracyjny
 *
 * Aplikacja prezentuje wywołanie funkcji subtractfromcard przez JDBC dla bazy danych PostgreSQL.
 *
 * Funkcja zwraca wartość reprezentującą róznicę salda karty po odjęciu kwoty podanej
 * w parameterze funkcji.
 *
 * Uruchomienie nie wymaga podawania parametrów
 */
public class CallableStatement_Example2_Application {

    public static void main(String[] args) throws Exception {
        //Nawiązanie połączenia (PostgreSQL + Hikari, parametry w pliku datasource.properties)
        var connection = HikariDatasourceConnection.getConnection();

        //Instrukcja dla wywołania FUNKCJI subtractfromcard
        String instruction = "{ ? = call public.subtractfromcard(?,?) }";

        try (var statement = connection.prepareCall(instruction)) {
            statement.setString(2, "1");
            statement.setFloat(3, 78.90f);
            statement.registerOutParameter(1, Types.REAL);
            statement.execute();
            var balance = statement.getFloat(1);

            System.out.println("Saldo karty po odjęciu 78.90 to " + balance);
        }

        //Zwolnienie połączenia
        connection.close();

        //Zwolnienie puli
        HikariDatasourceConnection.close();
    }
}
