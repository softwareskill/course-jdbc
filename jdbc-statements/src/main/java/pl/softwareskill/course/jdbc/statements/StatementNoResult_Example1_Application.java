package pl.softwareskill.course.jdbc.statements;

/**
 * Kod demonstracyjny
 *
 * Aplikacja prezentuje podstawowy tryb wywołania dla wyrażenia - execute
 *
 * Uruchomienie nie wymaga podawania parametrów
 */
public class StatementNoResult_Example1_Application {

    public static void main(String[] args) throws Exception {
        //Nawiązanie połączenia (PostgreSQL + Hikari, parametry w pliku datasource.properties)
        var connection = HikariDatasourceConnection.getConnection();

        try (var statement = connection.createStatement()) {
            //Zapytanie SQL
            String sqlQuery = "SELECT AMOUNT_TO_PAY FROM ORDERS WHERE ORDER_NUMBER='XYZ'";

            //Wykonanie zapytania
            var status = statement.execute(sqlQuery);

            //Wiemy że instrukcja to SELECT true oznacza możliwy wynik, który może być pusty
            if (status) {
                System.out.println("Zapytanie z możliwym do pobrania rezultatem ");
            } else {
                System.err.println("Zapytanie bez możliwości pobrania rezultatu");
            }
        }

        //Zwolnienie połączenia
        connection.close();

        //Zwolnienie puli
        HikariDatasourceConnection.close();
    }
}
