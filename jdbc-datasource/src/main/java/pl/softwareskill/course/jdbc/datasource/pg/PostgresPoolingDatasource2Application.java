package pl.softwareskill.course.jdbc.datasource.pg;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.sql.PooledConnection;
import org.postgresql.PGProperty;
import org.postgresql.ds.PGConnectionPoolDataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Kod demonstracyjny
 *
 * Aplikacja prezentująca połączenie do bazy danych z wykorzystaniem natywneg puli połączeń bazy danych PostgreSQL
 * (dostępne w ramach sterownika JDBC dla Postgres).
 *
 * Prezentuje taże możliwości konfiguracji tej natywnej puli z wykorzystaniem PGConnectionPoolDataSource
 *
 * Uruchomienie nie wymaga podawania parametrów
 */
 public class PostgresPoolingDatasource2Application {

    private static final Logger LOGGER = LoggerFactory.getLogger(PostgresPoolingDatasource2Application.class);

    public static void main(String[] args) throws SQLException, InterruptedException {
        //Kod demonstracyjny

        //Implementacja DataSource
        var dataSource = new PGConnectionPoolDataSource();

        //Ustawienie parametrów połączenia do bazy danych dla datasource
        dataSource.setDatabaseName("softwareskill");
        dataSource.setURL("jdbc:postgresql:softwareskill?loggerLevel=trace");
        dataSource.setUser("softwareskill");
        dataSource.setPassword("softwareskill");
        dataSource.setProperty(PGProperty.TCP_KEEP_ALIVE, "true");
        dataSource.setProperty(PGProperty.LOG_UNCLOSED_CONNECTIONS, "true");
        dataSource.setProperty(PGProperty.LOGGER_LEVEL, "trace");
//        dataSource.setDefaultAutoCommit(false); //blokuje wykonanie insert|update|delete

        List<PooledConnection> connections = new ArrayList<>();
        for (int i = 0; i < 3; i++) {
            //Nawiązanie połączenia z bazą danych
            var connection = dataSource.getPooledConnection();
            connection.getConnection().setAutoCommit(false);
            connections.add(connection);
            PostgresSessionChecker.initSessionCheckData(i, connection);
        }

        Thread.sleep(1000);

        PooledConnection pooledConnection = connections.get(0);
        PostgresSessionChecker.checkSessionData(pooledConnection);

        pooledConnection.getConnection().close();

        var connection = dataSource.getPooledConnection();
        LOGGER.info("Połączono do bazy danych z wykorzystaniem puli połączeń");
    }
}
