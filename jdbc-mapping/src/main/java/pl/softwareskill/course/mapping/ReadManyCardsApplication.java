package pl.softwareskill.course.mapping;

import java.sql.ResultSet;

/**
 * Kod demonstracyjny
 *
 * Aplikacja prezentuje wyszukiwanie kart z wykorzystaniem PreparedStatement i operatora IN dla SQL
 *
 * Uruchomienie nie wymaga podawania parametrów
 */
public class ReadManyCardsApplication {

    public static void main(String[] args) throws Exception {
        //Nawiązanie połączenia (PostgreSQL + Hikari, parametry w pliku datasource.properties)
        var connection = HikariDatasourceConnection.getConnection();

        //Fraza IN - znak zapytania dla każdej wartości, nie ma konieczności używania apostrofów
        final String query = "select * from cards where card_id IN (?,?)";

        try (var statement = connection.prepareStatement(query)) {
            //Wstrzyknięcie wartości parametru - sterownik JDBC wstrzyknie odpowiednią wartość i doda apostrofy
            statement.setString(1, "1");
            statement.setString(2, "2");

            try (ResultSet resultSet = statement.executeQuery()) {
                while (resultSet.next()) {
                    var id = resultSet.getString("CARD_ID");
                    var uuid = resultSet.getString("CARD_UUID");
                    var enabed = resultSet.getString("ENABLED");
                    var country = resultSet.getString("COUNTRY");
                    var balance = resultSet.getBigDecimal("CARD_BALANCE");

                    var text = String.format("Dane karty id=%s, uuid=%s, enabled=%s, country=%s, balance=%f",
                            id, uuid, enabed, country, balance);
                    System.out.println(text);
                }
            }
        }

        //Zwolnienie połączenia
        connection.close();

        //Zwolnienie puli
        HikariDatasourceConnection.close();
    }
}