package pl.softwareskill.course.jdbc.statements;

/**
 * Kod demonstracyjny
 *
 * Aplikacja prezentuje tryb wsadowy/batch  dla JDBC - dwa polecenia UPDATE oraz DELETE
 * oraz executeBatch
 *
 * Uruchomienie nie wymaga podawania parametrów
 */
public class StatementBatchExampleApplication {

    public static void main(String[] args) throws Exception {
        //Nawiązanie połączenia (PostgreSQL + Hikari, parametry w pliku datasource.properties)
        var connection = HikariDatasourceConnection.getConnection();

        try (var statement = connection.createStatement()) {
            //Zapytanie SQL
            String sqlQuery1 = "UPDATE ORDERS SET AMOUNT_TO_PAY=12.67";
            statement.addBatch(sqlQuery1);

            String sqlQuery2 = "DELETE FROM ORDERS WHERE ORDER_NUMBER='11111'";
            statement.addBatch(sqlQuery2);

            //Wykonanie zapytania z pobraniem liczby zmodyfikowanych/usuniętych wierszy
            var result = statement.executeBatch();
            for (int i = 0; i < result.length; i++) {
                System.out.println("Liczba dotkniętych wierszy dla polecenia " + i + " wynosi " + result[i]);
            }
        }

        //Zwolnienie połączenia
        connection.close();

        //Zwolnienie puli
        HikariDatasourceConnection.close();
    }
}
