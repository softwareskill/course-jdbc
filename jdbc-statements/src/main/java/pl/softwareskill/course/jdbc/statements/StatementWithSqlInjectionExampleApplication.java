package pl.softwareskill.course.jdbc.statements;

/**
 * Kod demonstracyjny
 *
 * Aplikacja prezentuje wstrzyknięcie złośliwego kodu SQL do zapytania dla wyrażenia prostego - SQL Injection
 *
 * Uruchomienie nie wymaga podawania parametrów
 */
public class StatementWithSqlInjectionExampleApplication {

    public static void main(String[] args) throws Exception {
        //Nawiązanie połączenia (PostgreSQL + Hikari, parametry w pliku datasource.properties)
        var connection = HikariDatasourceConnection.getConnection();

        //Symulacja wprowadzenia danych z formularza/linii poleceń
        //1. OR  '1'='1 zawsze prawda
        //2. AND  '1'<>'1 zawsze fałsz
        String infectedId = "nie ma znaczenia co i tak bedzie wynik'  OR  '1'='1";

        try (var statement = connection.createStatement()) {
            //Zapytanie SQL
            String sqlQuery = String.format(
                    "SELECT AMOUNT_TO_PAY FROM ORDERS WHERE ORDER_NUMBER='%s'",
                    infectedId); // wstrzyknięcie złośliwego SQLa do treści zapytania

            System.out.println("Treść zapytania = " + sqlQuery);

            //Wykonanie zapytania z rezultatem
            try (var resulSet = statement.executeQuery(sqlQuery)) {
                if (resulSet.next()) {
                    System.out.println("Znaleziono kwotę do zapłaty");
                } else {
                    System.err.println("Nie znaleziono kwoty do zapłaty");
                }
            }
        }

        //Zwolnienie połączenia
        connection.close();

        //Zwolnienie puli
        HikariDatasourceConnection.close();
    }
}
