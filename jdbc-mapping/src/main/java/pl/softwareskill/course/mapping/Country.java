package pl.softwareskill.course.mapping;

public enum Country {
    PL,
    DE,
    GB,
    FR
}
