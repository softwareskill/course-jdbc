package pl.softwareskill.course.jdbc.statements;

import java.sql.Array;

/**
 * Kod demonstracyjny
 *
 * Aplikacja prezentuje pobieranie typu ARRAY dla bazy danych PostgreSQL
 * oraz mapowanie tego typu przez sterownik JDBC
 *
 * Uruchomienie nie wymaga podawania parametrów
 */
public class StatementArrayExampleApplication {

    public static void main(String[] args) throws Exception {
        //Nawiązanie połączenia (PostgreSQL + Hikari, parametry w pliku datasource.properties)
        var connection = HikariDatasourceConnection.getConnection();

        try (var statement = connection.createStatement()) {
            //Zapytanie SQL
            String sqlQuery = "select ARRAY[ROW(1, 'foo'), ROW(2, 'baz'), ROW(3, 'foobar,')]";

            //Wykonanie zapytania
            var status = statement.execute(sqlQuery);

            //Pobranie wyniku
            try (var result = statement.getResultSet()) {

                if (status) {
                    if (result.next()) {

                        Array array = result.getArray(1);
                        System.out.println("Content value - array " + array);
                    }
                }
            }
        }

        //Zwolnienie połączenia
        connection.close();

        //Zwolnienie puli
        HikariDatasourceConnection.close();
    }
}
