package pl.softwareskill.course.jdbc.connection;

import java.sql.DriverManager;
import lombok.extern.slf4j.Slf4j;

/**
 * Aplikacja demonstracyjna
 * <p>
 * Funkcjonalność - połączenie do bazy danych PostgreSQL z włączonym poszerzonym logowaniem.
 * <p>
 * Aplikacja nie wymaga podania parametrów
 */
@Slf4j
public class ApplicationWithJdbcQueryTracing {

    public static void main(String[] args) throws Exception {
        //Kod demonstracyjny

        //Rejestracja klasy sterownika
        //Nie jest konieczne bo jest plik META-INF/services/java.sqlDriver w jar ze sterownikiem
        Class.forName("org.postgresql.Driver");

        //nawiązanie połączenia do bazy PostgreSQL
        var connection = DriverManager.getConnection(
                "jdbc:postgresql:softwareskill?loggerLevel=trace", //loggerLevel wartości info|debug|trace - wg. specyfikacji sterownika
                "softwareskill",
                "softwareskill");
        log.info("Pomyslnie zalogowano do bazy danych");

        connection.createStatement().executeQuery("Select 1 ");
    }
}
