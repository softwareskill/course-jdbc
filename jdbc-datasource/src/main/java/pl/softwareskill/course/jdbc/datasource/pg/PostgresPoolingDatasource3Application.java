package pl.softwareskill.course.jdbc.datasource.pg;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.postgresql.PGProperty;
import org.postgresql.ds.PGPoolingDataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Kod demonstracyjny
 *
 * Aplikacja prezentująca połączenie do bazy danych z wykorzystaniem natywneg puli połączeń bazy danych PostgreSQL
 * (dostępne w ramach sterownika JDBC dla Postgres).
 *
 * Prezentuje możliwości konfiguracji tej natywnej puli oraz reużywanie połączeń dla puli z wykorzyustaniem PGPoolingDataSource
 *
 * Uruchomienie nie wymaga podawania parametrów
 */
public class PostgresPoolingDatasource3Application {

    private static final Logger LOGGER = LoggerFactory.getLogger(PostgresPoolingDatasource3Application.class);

    public static void main(String[] args) throws SQLException, InterruptedException {
        //Kod demonstracyjny

        //Implementacja natywnego DataSource dla PostgreSQL
        @SuppressWarnings("deprecation")
        var dataSource = new PGPoolingDataSource();

        //Ustawienie parametrów połączenia do bazy danych dla datasource
        dataSource.setDatabaseName("softwareskill");
        dataSource.setURL("jdbc:postgresql:softwareskill?loggerLevel=trace");
        dataSource.setUser("softwareskill");
        dataSource.setPassword("softwareskill");
        dataSource.setProperty(PGProperty.TCP_KEEP_ALIVE, "true");
        dataSource.setProperty(PGProperty.LOG_UNCLOSED_CONNECTIONS, "true");
        dataSource.setProperty(PGProperty.LOGGER_LEVEL, "trace");
        dataSource.setMaxConnections(10);
        dataSource.setInitialConnections(10);

        List<Connection> connections = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            //Nawiązanie połączenia z bazą danych
            var connection = dataSource.getConnection();
            connections.add(connection);
        }

        Thread.sleep(1000);

        connections.get(0).close();

        var connection = dataSource.getConnection();
        LOGGER.info("Połączono do bazy danych z wykorzystaniem puli połączeń");
    }
}
