package pl.softwareskill.course.mapping;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Optional;

/**
 * Kod demonstracyjny
 *
 * Aplikacja prezentuje aktualizację danych klienta (dużo kolumn) karty z wykorzystaniem Statement (wyrażenie proste)
 *
 * Uruchomienie nie wymaga podawania parametrów
 */
public class UpdateCustomerByStatementApplication {

    public static void main(String[] args) throws Exception {
        //Nawiązanie połączenia (PostgreSQL + Hikari, parametry w pliku datasource.properties)
        var connection = HikariDatasourceConnection.getConnection();

        var updateDate = new Date();
        //Zapytanie dla wyrażenia prostego musi być przygotowanie - z apostrofami i odpowiednim formatowaniem czasu/daty
        final String updateSql = "UPDATE CUSTOMERS SET " +
                "    FIRST_NAME ='%s', " + //apostrofy
                "    LAST_NAME ='%s', " + //apostrofy
                "    EMAIL_ADDRESS ='%s', " + //apostrofy
                "    CARD_ID ='%s', " + //apostrofy
                "    STREET ='%s', " + //apostrofy
                "    STREET_NUMBER ='%s', " + //apostrofy
                "    CITY ='%s', " + //apostrofy
                "    COUNTRY ='%s', " + //apostrofy
                "    POSTAL_CODE ='%s', " + //apostrofy

                //Jeśli null, to nie ma zamykania tekstu w '' bo 'null' <>> null dla SQL
                "    UPDATED_AT =" + Optional.ofNullable(updateDate).map(date -> "'%s' ").orElse("%s ") + "," +

                " WHERE CUSTOMER_ID=%d";
        //Odpowiednie formatowanie daty dla bazy danych
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        final String updateWithData = String.format(updateSql,
                "Krzysztof",
                "Kadziolka",
                "krzysztof.kadziolka@gmail.com",
                "1",
                "Adresowa",
                "1A",
                "Katowice",
                "PL",
                "00-089",

                Optional.ofNullable(updateDate).map(format::format).orElse("null"), //Sformatowana Data w '' lub null

                1L);
        try (var statement = connection.createStatement()) {

            int updateCount = statement.executeUpdate(updateWithData);

            if (updateCount == 0) {
                System.err.println("Nie znaleziono danych do aktualizacji");
            } else {
                System.out.println("Zmodyfikowano pomyślnie dane");
            }
        }

        //Zwolnienie połączenia
        connection.close();

        //Zwolnienie puli
        HikariDatasourceConnection.close();
    }
}