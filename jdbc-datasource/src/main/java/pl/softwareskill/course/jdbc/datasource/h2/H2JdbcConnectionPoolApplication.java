package pl.softwareskill.course.jdbc.datasource.h2;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.h2.jdbcx.JdbcConnectionPool;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Kod demonstracyjny
 *
 * Aplikacja prezentująca połączenia do bazy danych z wykorzystaniem natywnej puli połączeń dla bazy danych H2
 * (dostępne w ramach sterownika JDBC dla H2). Pula ma ograniczony rozmiar. Aplikacja prezentuje zachowanie
 * natywnej puli połączeń w zależności od liczby aktywnych połączeń.
 *
 * Uruchomienie nie wymaga podawania parametrów
 */
public class H2JdbcConnectionPoolApplication {

    private static final Logger LOGGER = LoggerFactory.getLogger(H2JdbcConnectionPoolApplication.class);

    public static void main(String[] args) throws SQLException, InterruptedException {
        //Kod demonstracyjny

        //Implementacja DataSource
        var dataSource = JdbcConnectionPool.create("jdbc:h2:mem:softwareskill_orm;TRACE_LEVEL_SYSTEM_OUT=3;",
                "softwareskill", "softwareskill");
        dataSource.setMaxConnections(5); //ustawienie limitu na 5 połączen dla puli

        List<Connection> connections = new ArrayList<>();
        for (int i = 0; i < 5; i++) { // pobranie/nawiązanie 5 połączeń (rozmiar puli)
            //Nawiązanie połączenia z bazą danych
            Connection connection = dataSource.getConnection();
            connections.add(connection);
        }

        Thread.sleep(1000);

        connections.get(0).close();//zaknięcie pierwszego z połączeń

        var connection = dataSource.getConnection();//próba pobrania połączenia
        LOGGER.info("Połączono do bazy danych z wykorzystaniem puli połączeń");

        connection = dataSource.getConnection();//próba pobrania połączenia
        connection = dataSource.getConnection();//próba pobrania połączenia
    }
}
