package pl.softwareskill.course.crud;

import java.sql.Connection;
import java.sql.SQLException;
import static java.util.Objects.isNull;

/**
 * Kod demonstracyjny
 *
 * Aplikacja usuwa nieopłacone zamówienie
 * 1. Sprawdzenie istnienia oraz statusu zamówienia
 * 2. Jeżeli status WAITING_FOR_PAYMENT to usunięcie zamówienia
 *
 * W przypadku błędu wyjście z programu z komunikatem błędu.
 *
 * Uruchomienie nie wymaga podawania parametrów
 */
public class DeleteUnpaidOrderApplication {

    public static void main(String[] args) throws Exception {
        //Nawiązanie połączenia
        var connection = CrudDatasource.connectToDatabase();

        //rozpoczęcie transakcji JDBC
        connection.setAutoCommit(false);

        //Pobranie statusu zamówienia
        var orderStatus = getOrderStatus(connection);

        //Weryfikacja statusu zamówienia
        if (validateStatus(orderStatus)) {
            connection.rollback();
            System.exit(-1);
        }

        if ("WAITING_FOR_PAYMENT".equals(orderStatus)) {
            try (var statement = connection.createStatement()) {
                //CRUD DELETE/DESTROY - zapytanie SQL
                String sqlQuery = "DELETE FROM ORDERS WHERE ORDER_NUMBER='TO_DELETE'";

                //Wykonanie zapytania
                statement.execute(sqlQuery);

                //Commit JDBC
                connection.commit();

                System.out.println("Zamówienie usunięte");
            }
        }
    }

    private static String getOrderStatus(Connection connection) throws SQLException {
        try (var statement = connection.createStatement()) {
            //CRUD READ/RETRIEVE - zapytanie SQL
            String sqlQuery = "SELECT STATUS FROM ORDERS WHERE ORDER_NUMBER='TO_DELETE'";

            //Wykonanie zapytania
            statement.execute(sqlQuery);

            //Weryfikacja wyniku
            try (var resultSet = statement.getResultSet()) {

                //Pobieramy wartość
                if (resultSet.next()) { //Zamówienie znalezione
                    //Zwracamy status
                    return resultSet.getString(1);
                } else {
                    return null;
                }
            }
        }
    }

    private static boolean validateStatus(String orderStatus) {
        if (isNull(orderStatus)) {
            System.err.println("Zamówienie nie istnieje");
            return false;
        } else {
            return !"PAID".equals(orderStatus);
        }
    }
}
