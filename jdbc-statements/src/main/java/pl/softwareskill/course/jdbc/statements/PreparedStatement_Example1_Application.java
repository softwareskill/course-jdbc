package pl.softwareskill.course.jdbc.statements;

/**
 * Kod demonstracyjny
 *
 * Aplikacja prezentuje wyszukiwanie kwoty zamówienia dla bazy danych PostgreSQL
 * (tabela ORDERS) z wykorzystaniem wyrażenia prekompilowanego (PreparedStatement)
 * i trybu wywołania z wynikiem executeQuery
 *
 * Uruchomienie nie wymaga podawania parametrów
 */
public class PreparedStatement_Example1_Application {

    public static void main(String[] args) throws Exception {
        //Nawiązanie połączenia (PostgreSQL + Hikari, parametry w pliku datasource.properties)
        var connection = HikariDatasourceConnection.getConnection();

        //Zapytanie SQL - znak zapytania dla wstrzyknięcia parametru, apostrofy dodane zostaną przez sterownik
        String sqlQuery = "SELECT AMOUNT_TO_PAY FROM ORDERS WHERE ORDER_NUMBER=?";

        try (var statement = connection.prepareStatement(sqlQuery)) {

            //Ustawienie parametrów
            statement.setString(1, "1");

            //Wykonanie zapytania z wynikiem
            try (var result = statement.executeQuery()) {

                //Wiemy że instrukcja to SELECT true oznacza wynik, false brak
                if (result.next()) {
                    System.out.println("Znaleziono kwotę do zapłaty " + result.getBigDecimal(1));
                } else {
                    System.err.println("Nie znaleziono kwoty do zapłaty");
                }
            }
        }

        //Zwolnienie połączenia
        connection.close();

        //Zwolnienie puli
        HikariDatasourceConnection.close();
    }
}
