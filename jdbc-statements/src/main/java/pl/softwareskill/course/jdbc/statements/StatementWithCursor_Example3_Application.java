package pl.softwareskill.course.jdbc.statements;

import java.sql.ResultSet;

/**
 * Kod demonstracyjny
 *
 * Aplikacja prezentuje tryb wywołania wyrażenia z kursorem oraz przechodzenie po wyniku zapytania
 *
 * Uruchomienie nie wymaga podawania parametrów
 */
public class StatementWithCursor_Example3_Application {

    public static void main(String[] args) throws Exception {
        //Nawiązanie połączenia (PostgreSQL + Hikari + Sterownik P6Spy dla poszerzonego logowanias, parametry w pliku datasource_spy.properties)
        var connection = HikariDatasourceConnection.getTracingConnection();

        //Bez ustawiania ResultSet.CONCUR_UPDATABLE wywali się
        try (var statement = connection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE)) {

            //Zapytanie SQL
            String sqlQuery = "SELECT * FROM ORDERS";

            //Nazwa kursora
            statement.setCursorName("softwareskill");

            //Wykonanie zapytania z rezultatem
            try (var resulSet = statement.executeQuery(sqlQuery)) {

                if (resulSet.next()) { //Nie ma wpisu więc dodajemy nowy

                    if (resulSet.next()) {
                        resulSet.relative(3);
                        resulSet.absolute(1);
                    }
                }
            }
        }

        //Zwolnienie połączenia
        connection.close();

        //Zwolnienie puli
        HikariDatasourceConnection.close();
    }
}
