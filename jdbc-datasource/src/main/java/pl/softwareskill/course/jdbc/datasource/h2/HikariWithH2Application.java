package pl.softwareskill.course.jdbc.datasource.h2;

import com.zaxxer.hikari.HikariDataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Kod demonstracyjny
 *
 * Aplikacja prezentująca połączenia do bazy danych z wykorzystaniem puli połączeń Hikari i bazy danych H2.
 *
 * Pula ma ograniczony rozmiar. Aplikacja prezentuje zachowaniepuli połączeń Hikary w zależności od liczby aktywnych połączeń
 * Hikari ma wstrzyknietą konfigurację dla POLACZENIA - driverClassName - zachowanie jak źródło danych (zwalnianie i nawiązywanie
 * połączeń).
 *
 * Uruchomienie nie wymaga podawania parametrów
 */
public class HikariWithH2Application {

    private static final Logger LOGGER = LoggerFactory.getLogger(HikariWithH2Application.class);

    public static void main(String[] args) throws SQLException, InterruptedException {
        //Kod demonstracyjny

        //Implementacja DataSource
        var dataSource = new HikariDataSource();

        //Ustawienie parametrów połączenia do bazy danych dla datasource
        dataSource.setDriverClassName("org.h2.Driver");
        dataSource.setJdbcUrl("jdbc:h2:mem:softwareskill_orm;TRACE_LEVEL_SYSTEM_OUT=3");
        dataSource.setUsername("ormowner");
        dataSource.setPassword("");
        dataSource.setMaximumPoolSize(10);//maksymalny rozmiar puli 10
        dataSource.setMinimumIdle(10);//początkowy rozmiar puli = 10 == max rozm puli

        List<Connection> connections = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            //Nawiązanie połączenia z bazą danych
            Connection connection = dataSource.getConnection();
            connections.add(connection);
        }

        Thread.sleep(1000);

        //Wyczyszczenie pierwszego połączenia
        connections.stream()
                .findFirst()
                .ifPresent(dataSource::evictConnection);

        var connection = dataSource.getConnection();//próba pobrania połączenia
        LOGGER.info("Połączono do bazy danych z wykorzystaniem puli połączeń");
    }
}
