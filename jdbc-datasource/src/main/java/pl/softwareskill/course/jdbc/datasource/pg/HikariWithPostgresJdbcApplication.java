package pl.softwareskill.course.jdbc.datasource.pg;

import com.zaxxer.hikari.HikariDataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Kod demonstracyjny
 *
 * Aplikacja prezentująca połączenia do bazy danych z wykorzystaniem puli połączeń Hikari i bazy danych PostgreSQL.
 *
 * Pula ma ograniczony rozmiar. Aplikacja prezentuje zachowanie puli połączeń Hikary w zależności od liczby aktywnych połączeń
 * Hikari ma wstrzyknietą konfigurację dla CONNECTION -driverClassName - zachowanie jak żródło danych (połączenie
 * nie jest reużywane tylko zamykane i tworzone na nowo).
 *
 * Uruchomienie nie wymaga podawania parametrów
 */
public class HikariWithPostgresJdbcApplication {

    private static final Logger LOGGER = LoggerFactory.getLogger(HikariWithPostgresJdbcApplication.class);

    public static void main(String[] args) throws SQLException, InterruptedException {
        //Kod demonstracyjny

        //Implementacja DataSource
        var dataSource = new HikariDataSource();

        //Ustawienie parametrów połączenia do bazy danych dla datasource
        dataSource.setDriverClassName("org.postgresql.Driver");
        dataSource.setJdbcUrl("jdbc:postgresql:softwareskill?loggerLevel=trace");
        dataSource.setUsername("softwareskill");
        dataSource.setPassword("softwareskill");
        dataSource.setMaximumPoolSize(10);
        dataSource.setMinimumIdle(0);

        List<Connection> connections = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            //Nawiązanie połączenia z bazą danych
            Connection connection = dataSource.getConnection();
            connection.setAutoCommit(false);
            //Ustawienie parametrów sesji
            PostgresSessionChecker.initSessionCheckData(i, connection);
            connections.add(connection);
        }

        Thread.sleep(1000);

        //Zwolnienie pierwszego połączenias
        connections.stream()
                .findFirst()
                .ifPresent(dataSource::evictConnection);

        var connection = dataSource.getConnection();//Próba pobrania kolejnego połączenia

        //Sprawdzenie wartości parametru sesji
        PostgresSessionChecker.checkSessionData(connection);
        LOGGER.info("Połączono do bazy danych z wykorzystaniem puli połączeń");

        dataSource.close();
    }
}
