package pl.softwareskill.course.crud;

import com.zaxxer.hikari.HikariDataSource;
import java.sql.Connection;
import java.sql.SQLException;

public class CrudDatasource {

    public static Connection connectToDatabase() throws SQLException {
        //Kod demonstracyjny

        //DataSource
        var dataSource = new HikariDataSource();

        //Ustawienie parametrów połączenia do bazy danych dla datasource
        dataSource.setDriverClassName("org.h2.Driver");
        dataSource.setJdbcUrl("jdbc:h2:mem:softwareskill_orm;DB_CLOSE_DELAY=-1;INIT=RUNSCRIPT FROM 'classpath:crud_example_database.sql'");
        dataSource.setUsername("ormowner");
        dataSource.setPassword("");

        //Maksymalny rozmiar puli połączeń
        dataSource.setMaximumPoolSize(6);

        //Nawiązanie połączenia z bazą danych
        return dataSource.getConnection();
    }
}
