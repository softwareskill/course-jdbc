package pl.softwareskill.course.jdbc.statements;

import java.time.Instant;

/**
 * Kod demonstracyjny
 *
 * Aplikacja prezentuje pobieranie obiektu (typ natywny dla bazy danych - tabela w kolumnie
 * przechowuje obiekt złożony o typie zdefiniowanycm przez użytkownika - address)
 * dla bazy danych PostgreSQL
 *
 * Uruchomienie nie wymaga podawania parametrów
 */
public class StatementObjectExampleApplication {

    public static void main(String[] args) throws Exception {
        //Nawiązanie połączenia
        var connection = HikariDatasourceConnection.getConnection();

        try (var statement = connection.createStatement()) {
            //Zapytanie SQL
            String sqlQuery = "select living_address, name FROM PERSON";

            //Wykonanie zapytania
            var status = statement.execute(sqlQuery);

            //Pobranie wyniku
            try (var result = statement.getResultSet()) {

                if (status) {
                    if (result.next()) {
                        //Pobranie wartości kolumny, która reprezentuje własny typ użytkownika - typ address
                        var object = result.getObject(1);
                        System.out.println("Content value - object " + object);
                    }
                }
            }
        }

        //Mertoda nie obsługiwana w PostgreSQL
        var address = connection.createStruct("address", new Object[]{"polna", "78-900"});

        try (var statement = connection.prepareStatement("insert into PERSON(address,name) values (?,?")) {
            statement.setObject(1, address);
            statement.setString(2, String.valueOf(Instant.now().toEpochMilli()));

            statement.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }

        //Zwolnienie połączenia
        connection.close();

        //Zwolnienie puli
        HikariDatasourceConnection.close();
    }
}
