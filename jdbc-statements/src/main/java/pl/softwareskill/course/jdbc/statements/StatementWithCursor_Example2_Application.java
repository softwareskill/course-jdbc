package pl.softwareskill.course.jdbc.statements;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.time.Instant;

/**
 * Kod demonstracyjny
 *
 * Aplikacja prezentuje tryb wywołania wyrażenia z kursorem oraz dodaniem wiersza do ResultSet dla sterownika PostgreSQL
 *
 * Rozszerzone logowanie pozawala na weryfikację jakie zapytania zostaną wysłane do bazy danych.
 *
 * Uruchomienie nie wymaga podawania parametrów
 */
public class StatementWithCursor_Example2_Application {

    public static void main(String[] args) throws Exception {
        //Nawiązanie połączenia (PostgreSQL + Hikari + Sterownik P6Spy dla poszerzonego logowanias, parametry w pliku datasource_spy.properties)
        var connection = HikariDatasourceConnection.getTracingConnection();

        //Bez ustawiania ResultSet.CONCUR_UPDATABLE wywali się
        try (var statement = connection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE)) {

            //Zapytanie SQL
            String sqlQuery = "SELECT ORDER_NUMBER,AMOUNT_TO_PAY,ORDER_STATUS FROM ORDERS WHERE ORDER_NUMBER='%s'";

            //Nowy unikalny id na podstawie czasu
            final long uniqueId = Instant.now().toEpochMilli();

            sqlQuery = String.format(sqlQuery, uniqueId);

            //Nazwa kursora
            statement.setCursorName("softwareskill");

            //Wykonanie zapytania z rezultatem
            try (var resulSet = statement.executeQuery(sqlQuery)) {

                if (!resulSet.next()) { //Nie ma wpisu więc dodajemy nowy

                    //Przesuwamy się do wierszy ddoawanych
//                    resulSet.moveToInsertRow();

                    resulSet.updateString(1, String.valueOf(uniqueId));
                    resulSet.updateBigDecimal(2, BigDecimal.valueOf(uniqueId));
                    resulSet.updateString(3, "WAITING_FOR_PAYMENT");

                    resulSet.insertRow();

                    var isInserted = resulSet.rowInserted();//Dla Postgresql nie zadziała

                    System.out.println("Dodano zamówienie");
                } else {
                    System.err.println("Zamówienie już istnieje");
                }
            }
        }

        //Zwolnienie połączenia
        connection.close();

        //Zwolnienie puli
        HikariDatasourceConnection.close();
    }
}
