package pl.softwareskill.course.jdbc.datasource.pg;

import com.zaxxer.hikari.HikariDataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Kod demonstracyjny
 *
 * Aplikacja prezentująca połączenia do bazy danych z wykorzystaniem puli połączeń Hikari i bazy danych PostgreSQL.
 *
 * Pula ma ograniczony rozmiar. Aplikacja prezentuje zachowanie puli połączeń Hikary w zależności od liczby aktywnych połączeń
 * Hikari ma wstrzyknietą konfigurację dla DATASOURCE - datasourceClassName - zachowanie jak pula połączeń (reużywanie połączeń).
 *
 * Uruchomienie nie wymaga podawania parametrów
 */
public class HikariWithPostgresDatasourceApplication {

    private static final Logger LOGGER = LoggerFactory.getLogger(HikariWithPostgresDatasourceApplication.class);

    public static void main(String[] args) throws SQLException, InterruptedException {
        //Kod demonstracyjny

        //Implementacja DataSource
        var dataSource = new HikariDataSource();

        //Ustawienie parametrów połączenia do bazy danych dla datasource
        dataSource.setDataSourceClassName("org.postgresql.ds.PGSimpleDataSource");
        dataSource.setJdbcUrl("jdbc:postgresql:softwareskill");
        dataSource.setUsername("softwareskill");
        dataSource.setPassword("softwareskill");

        dataSource.setMaximumPoolSize(10);
        dataSource.setMinimumIdle(0);

        dataSource.setMaxLifetime(1000);
        dataSource.setIdleTimeout(1000);

        Properties properties = new Properties();
        properties.put("loggerLevel", "trace");
        dataSource.setDataSourceProperties(properties);

        List<Connection> connections = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            //Nawiązanie połączenia z bazą danych
            Connection connection = dataSource.getConnection();
            connection.setAutoCommit(false);
            //ustawienie paremetrów sesji dla połączenia
            PostgresSessionChecker.initSessionCheckData(i, connection);
            connections.add(connection);
        }

        Thread.sleep(5000);

        LOGGER.info("PRZED ZAMKNIECIEM");
        connections.get(0).close();//zamknięcie połączenia
        LOGGER.info("PO ZAMKNIECIU");

        var connection = dataSource.getConnection();
        LOGGER.info("Połączono do bazy danych z wykorzystaniem puli połączeń");

        //Weryfikacja wartości parametru sesji dla reużywanego połączenia
        PostgresSessionChecker.checkSessionData(connection);

        dataSource.close();
    }
}
