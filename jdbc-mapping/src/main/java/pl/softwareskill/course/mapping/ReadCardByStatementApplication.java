package pl.softwareskill.course.mapping;

import java.sql.ResultSet;
import java.sql.Statement;

/**
 * Kod demonstracyjny
 *
 * Aplikacja prezentuje wyszukiwanie karty z wykorzystaniem Statement (wyrażenie proste)
 *
 * Uruchomienie nie wymaga podawania parametrów
 */
public class ReadCardByStatementApplication {

    public static void main(String[] args) throws Exception {
        //Nawiązanie połączenia (PostgreSQL + Hikari, parametry w pliku datasource.properties)
        var connection = HikariDatasourceConnection.getConnection();

        //Utworzenie wyrażenia prostego
        try (Statement statement = connection.createStatement()) {
            //Przygotowanie zapytania wymaga dla wyrażenie prostego stworzenia całego SQLa z apostrofami
            statement.execute(String.format("select * from cards where card_id='%s'", "1"));

            try (ResultSet resultSet = statement.getResultSet()) {
                if (resultSet.next()) {
                    var id = resultSet.getString("CARD_ID");
                    var uuid = resultSet.getString("CARD_UUID");
                    var enabed = resultSet.getString("ENABLED");
                    var country = resultSet.getString("COUNTRY");
                    var balance = resultSet.getBigDecimal("CARD_BALANCE");

                    var text = String.format("Dane karty id=%s, uuid=%s, enabled=%s, country=%s, balance=%f",
                            id, uuid, enabed, country, balance);
                    System.out.println(text);
                } else {
                    System.err.println("Nie znaleziono karty");
                }
            }
        }

        //Zwolnienie połączenia
        connection.close();

        //Zwolnienie puli
        HikariDatasourceConnection.close();
    }
}